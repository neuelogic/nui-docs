# Nui Documentation

This repository contains the documentation for most all Nui-based modules.

This repo is deployed to the docs sections of the NeueLogic website.

## Structure

Below are rough outlines of the markdown pages for each section that are either in the `planned`, `draft`, or `publishable` state.

### Code Sections

* [/code/](/code/) ***`draft`***
* **Developer Reference**
	* [/code/not-found](/code/not-found) *`planned`*
	* [/code/document](/code/document) ***`draft`***
	* [/code/page-structure](/code/page-structure) ***`draft`*** ***DEPRECATED***
	* [/code/nui](/code/nui) *`planned`*
	* [/code/nui/resource-ref](/code/nui/resource-ref) *`planned`*
	* [/code/nui/router](/code/nui/router) *`planned`*
	* [/code/nui/types](/code/nui/types) *`planned`*
	* **Actions**
		* [/code/nui/action](/code/nui/action) *`planned`*
		* [/code/nui/types/action](/code/nui/types/action) *`planned`*
		* [/code/nui/interfaces/action](/code/nui/interfaces/action) *`planned`*
	* **Stores**
		* [/code/nui/store](/code/nui/store) *`planned`*
		* [/code/nui/types/store](/code/nui/types/store) *`planned`*
		* [/code/nui/interfaces/store](/code/nui/interfaces/store) *`planned`*
	* **Views**
		* [/code/nui/view](/code/nui/view) *`planned`*
	* **Pages**
		* [/code/nui/page](/code/nui/page) *`planned`*
	* **Views And Pages** *(Components)*
		* [/code/nui/types/component](/code/nui/types/component) *`planned`*
		* [/code/nui/interfaces/component](/code/nui/interfaces/component) *`planned`*
		* [/code/nui/types/component/](/code/nui/types/component/) *`redirects to /nui/view (?)`*
			* [/code/nui/types/component/Actions](/code/nui/types/component/Actions) *`planned`*
			* [/code/nui/types/component/Stores](/code/nui/types/component/Stores) *`planned`*
			* [/code/nui/types/component/Views](/code/nui/types/component/Views) *`planned`*
			* [/code/nui/types/component/lifecycle](/code/nui/types/component/lifecycle) *`planned`*
			* [/code/nui/types/component/render](/code/nui/types/component/render) *`planned`*
			* [/code/nui/types/component/renderWaiting](/code/nui/types/component/renderWaiting) *`planned`*
		* **React**
			* [/code/nui/types/component/react/componentDidMount](/code/nui/types/component/react/componentDidMount) *`planned`*
			* [/code/nui/types/component/react/componentDidUpdate](/code/nui/types/component/react/componentDidUpdate) *`planned`*
			* [/code/nui/types/component/react/componentWillMount](/code/nui/types/component/react/componentWillMount) *`planned`*
			* [/code/nui/types/component/react/componentWillReceiveProps](/code/nui/types/component/react/componentWillReceiveProps) *`planned`*
			* [/code/nui/types/component/react/componentWillUnmount](/code/nui/types/component/react/componentWillUnmount) *`planned`*
			* [/code/nui/types/component/react/componentWillUpdate](/code/nui/types/component/react/componentWillUpdate) *`planned`*
			* [/code/nui/types/component/react/shouldComponentUpdate](/code/nui/types/component/react/shouldComponentUpdate) *`planned`*
* **Modules**
	* [/code/modules/build](/code/modules/build) *`planned`*
	* [/code/modules/build-watch](/code/modules/build-watch) *`planned`*
	* [/code/modules/builder-babel](/code/modules/builder-babel) *`planned`*
	* [/code/modules/logger](/code/modules/logger) *`planned`*
	* [/code/modules/nui](/code/modules/nui) *`planned`*
		* [/code/modules/nui/neueui](/code/modules/nui/neueui) *`planned`*
	* [/code/modules/platform-browser](/code/modules/platform-browser) *`planned`*
	* [/code/modules/platform-node](/code/modules/platform-node) *`planned`*
	* [/code/modules/simple](/code/modules/simple) *`planned`*
	* [/code/modules/utils](/code/modules/utils) *`planned`*
* **Packages**
	* [/code/pkgs/markdown-pages](/code/pkgs/markdown-pages) *`planned`*


### Learn Sections

* [/learn](/learn) *`planned`*
* **Beginner**
	* **Getting Started**
		* [/learn/contribute](/learn/contribute) *`planned`*
		* [/learn/styling-tips](/learn/styling-tips) *`planned`*
		* [/learn/quick-start](/learn/quick-start) *`planned`*
		* [/learn/example-app](/learn/example-app) *`planned`*
		* [/learn/first-page](/learn/first-page) *`planned`*
		* [/learn/first-component](/learn/first-component) *`planned`*
		* [/learn/project-structure](/learn/project-structure) *`planned`*
	* **Concepts**
		* [/learn/smart-vs-dumb-components](/learn/smart-vs-dumb-components) ***`publishable`***
		* [/learn/how-dependencies-work](/learn/how-dependencies-work) *`planned`*
	* **Resource Articles**
		* [/learn/resources](/learn/resources) *`planned`*
		* [/learn/resources/what-are-they](/learn/resources/what-are-they) *`planned`*
		* [/learn/resources/actions](/learn/resources/actions) *`planned`*
		* [/learn/resources/stores](/learn/resources/stores) *`planned`*
		* [/learn/resources/pages](/learn/resources/pages) *`planned`*
		* [/learn/resources/views](/learn/resources/views) *`planned`*
* **Intermediate**
	* *`planned`*
* **Advanced**
	* *`planned`*
* **Contributor**
	* [/learn/contribute](/learn/contribute) *`planned`*
