[Code](/code/) > [Nui](/code/nui).[Types](/code/nui/types).[Components](/code/nui/types/component)

# `Class` Nui.Types.Component

See also [Nui Components Basics](/learn/nui-basics/components)

See also [Smart vs Dumb Components](/learn/smart-vs-dumb-components)

<!-- articlecontent -->
A **`Component`** is the presentation portion of any `React.js` application, and in `nui` they inherit functionality from React's `Component` class.

In `nui`, there are two component types: **`views`** and **`pages`**.

These components are considered *`Smart Components`*. *For performance reasons, these should only be used to implement functionality.* A "dumb" component should typically be used in most cases unless there's significant logic. Read up on [*`Dumb Components`*](/learn/smart-vs-dumb-components).

## Syntax

```javascript
export const Routes = ''; /* (pages only) */
export default Nui => class ComponentName extends Nui.Types.Component {
	render()
}
```

### Exports

##### `default` *`view`* *`page`*
>> *`function`* defines and returns the component
>> *`param`* *`object`* `nui`: See [component interface](/code/nui/interfaces/component)
##### `Routes` *`page`*
>> One of the following:
>> * *`string`* Defines a static path to render the page.
>> * *`regexp`* Renders page if `pathname` matches `regexp`.
>> * *`array`* An array of strings or regular expressions, functioning above
>>
>> *All routes are absolute paths.*
>>
>>
>> Read up on [`Routing`](https://github.com/ReactTraining/react-router/blob/v3.2.0/docs/guides/RouteMatching.md).

## Class Specification

The methods below can be defined to take advantage of various aspects of the framework.

### Properties

###### [`.state`](/code/nui/types/component/render)
> *`object`* Contains the component's state

###### [`.propTypes`](/code/nui/types/component/render)
> *`static`* *`object`* Defines the required types that each property should be.

###### [`.defaultProps`](/code/nui/types/component/render)
> *`static`* *`object`* Defines the default property values. Used when no props are passed in by the parent component.

### Methods

###### [`.render()`](/code/nui/types/component/render) *`nui`*
> Used to render the component
> Should return a `Nui` or `React` component, or `null`.


###### [`.renderWaiting()`](/code/nui/types/component/renderWaiting) *`nui`*
> Used to render the component when its dependencies are not ready
> Should return a `Nui` or `React` component, or `null`.
> *See [How Dependencies Work](/code/nui/types/component).*


###### [`.componentWillMount()`](/code/nui/types/component/react/componentWillMount) *`react`*
> Executes once *before* component is about to be rendered for the first time


###### [`.componentDidMount()`](/code/nui/types/component/react/componentDidMount) *`react`*
> Executes once *after* component has been rendered for the first time


###### [`.componentWillReceiveProps()`](/code/nui/types/component/react/componentWillReceiveProps) *`react`*
> Executes when a component's *`props`* will change


###### [`.shouldComponentUpdate()`](/code/nui/types/component/react/shouldComponentUpdate) *`react`*
> Executes when *`React`* is attempting to determine whether the component needs to be re-rendered
> Should return a boolean, *`true`* or *`false`*, depending on whether or not a re-render is necessary


###### [`.componentWillUpdate()`](/code/nui/types/component/react/componentWillUpdate) *`react`*
> Executes *before* component is about to be re-render


###### [`.componentDidUpdate()`](/code/nui/types/component/react/componentDidUpdate) *`react`*
> Executes *after* component has re-rendered


###### [`.componentWillUnmount()`](/code/nui/types/component/react/componentWillUnmount) *`react`*
> Executes once *before* component is about to be removed
> Useful for cleaning state and garbage collecting
> OOP Developers: Similar to `destruct` methods


## Instance Members

Instance Members are methods and properties that are defined by the `type`'s Initializer during importation. These properties and methods are accessible on the instance, but should not be defined manually.

### Properties
###### [`this.ref`](/code/nui/ResourceRef)
> *`ResourceRef`* The fully qualified reference name for the component.

###### [*this.nui_Ready*](/code/nui/types/component/lifecycle)
> **Warning, Do Not Use: Internal APIs are not guaranteed under `semver`.**
> *`boolean`* Defines whether or not the component is ready to render.

###### [*this.nui_depsReady*](/code/nui/types/component/lifecycle)
> **Warning, Do Not Use: Internal APIs are not guaranteed under `semver`.**
> *`boolean`* Defines whether or not all the component's dependencies have fulfilled their `ready` state.

### Methods

###### [`this.Actions()`](/code/nui/types/component/view/Actions)
> Returns the specified action creator.
> *See [How Dependencies Work](/code/nui/types/component).*

###### [`this.Stores()`](/code/nui/types/component/view/Stores)
> Returns the interface for the specified Store.
> *See [How Dependencies Work](/code/nui/types/component).*

###### [`this.Views()`](/code/nui/types/component/view/Views)
> Returns the specified *`Component`*.
> *See [How Dependencies Work](/code/nui/types/component).*


## Nui Interface

`Nui Interface` describes the aspects of the framework instance available for access within the component.

###### Core Objects
> [**`Nui.Config`**](/code/nui.html#config)

> [**`Nui.Logger`**](/code/pkgs/logger)

> [**`Nui.Routesr`**](/code/nui/router)

###### Types
> [**`Nui.Types.Component`**](/code/nui/types/component)

###### Resource Getters
> [**`Nui.Actions()`**](/code/nui.html#get)

> [**`Nui.Stores()`**](/code/nui.html#get)

> [**`Nui.Views()`**](/code/nui.html#get)

> [**`Nui.get()`**](/code/nui.html#get)

###### Methods
> [**`Nui.register()`**](/code/nui.html#register)


## Example Component

```javascript
import React from 'react';

export const Routes = '/';

export default Nui => class HomePage extends Nui.Types.Component {
	render() {
		const Header = this.Views('global/header');
		const Footer = this.Views('global/footer');
		return (
			<section>
				<Header />
				<p>Hello world!</p>
				<Footer />
			</section>
		);
	}
}
```
<!-- endarticlecontent -->

<!-- seealso -->
##### See Also

* [**Nui.Types.Component**](/code/nui/types/component)
	* [`Static.propTypes`](/code/nui/types/component/render)
	* [`Static.defaultProps`](/code/nui/types/component/render)
	* [`Instance.ref`](/code/nui/ResourceRef)
	* [`Instance.Actions()`](/code/nui/types/component/view/Actions)
	* [`Instance.Stores()`](/code/nui/types/component/view/Stores)
	* [`Instance.Views()`](/code/nui/types/component/view/Views)
	* [`Prototype.state`](/code/nui/types/component/render)
	* [`Prototype.render()`](/code/nui/types/component/render)
	* [`Prototype.renderWaiting()`](/code/nui/types/component/renderWaiting)

**Inheritance:**
* [**React.Component**](/code/nui/types/component)
	* [`Prototype.componentWillMount()`](/code/nui/types/component/react/componentWillMount)
	* [`Prototype.componentDidMount()`](/code/nui/types/component/react/componentDidMount)
	* [`Prototype.componentWillReceiveProps()`](/code/nui/types/component/react/componentWillReceiveProps)
	* [`Prototype.shouldComponentUpdate()`](/code/nui/types/component/react/shouldComponentUpdate)
	* [`Prototype.componentWillUpdate()`](/code/nui/types/component/react/componentWillUpdate)
	* [`Prototype.componentDidUpdate()`](/code/nui/types/component/react/componentDidUpdate)
	* [`Prototype.componentWillUnmount()`](/code/nui/types/component/react/componentWillUnmount)

<!-- endseealso -->

<!-- topics -->
* [Creating Your First Component](/code/nui/types/component)
* [Creating Your First Page](/code/nui/types/component)
* [How Dependencies Work](/code/nui/types/component)
* [Smart vs Dumb Components](/code/nui/types/component)
* [Todo App Example](/code/nui/types/component)
<!-- endtopics -->
