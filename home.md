##### Simple, Module-Driven FLUX Framework

### Simple, Predictable

```javascript
/pages/ /*Pages are views that assign themselves to routes*/
/views/ /*Views make up your UI*/
/actions/ /*Actions implement functionality, like talking to APIs.*/
/stores/ /*Stores maintain your app's state.*/
```

[Check out a quick demonstration](/demo)

### Get Started

```bash
$ git clone git@bitbucket.org:bluelogicteam/todo-app.git nuiTodoExample
```

### Packages

The following are the main packages:

* [**`nui`**](/packages/nui) -- Core Framework
* [**`nui-build`**](/packages/nui-build) -- Bundler
* [**`nui-platform-node`**](/packages/nui-platform-node) -- Used for loading and rendering assets in a Node.JS environment.
* [**`nui-platform-browser`**](/packages/nui-platform-browser) -- Used for loading and rendering assets in a browser environment.

Other packages include:

* [**`nui-pkg-markdown`**](/packages/nui-pkg-markdown) -- A simple Nui package for serving and rendering markdown documents from a folder.
* [**`nui-utils`**](/packages/nui-utils) -- Contains useful utilities.
* [**`nui-builder-babel`**](/packages/nui-builder-babel) -- Used by `nui-build` for ES6 pre-compiling before bundling.

### Example Component

Example Component (really just to show off `nui-pkg-markdown`'s syntax highlighting :P )

```javascript
export default (NeueUI) => class HelloWorld extends NeueUI.Component {
	render() {
		return (
			<p>Hello World!</p>
		);
	}
}
```

---------------

This site was built using `nui` with `nui-pkg-markdown`.
